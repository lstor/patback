defmodule Patback.PageController do
  use Patback.Web, :controller

  plug :action

  def index(conn, _params) do
    redirect conn, to: "/feedback"
  end
end
