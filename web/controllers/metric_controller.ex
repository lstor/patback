defmodule Patback.MetricController do
  use Patback.Web, :controller

  alias Patback.Metric

  plug :scrub_params, "metric" when action in [:create, :update]
  plug :action

  def index(conn, _params) do
    metrics = Repo.all(Metric)
    render conn, "index.html", metrics: metrics
  end

  def new(conn, _params) do
    changeset = Metric.changeset(%Metric{})
    render conn, "new.html", changeset: changeset
  end

  def create(conn, %{"metric" => metric_params}) do
    changeset = Metric.changeset(%Metric{}, metric_params)

    if changeset.valid? do
      Repo.insert(changeset)

      conn
      |> put_flash(:info, "Metric created successfully.")
      |> redirect(to: metric_path(conn, :index))
    else
      render conn, "new.html", changeset: changeset
    end
  end

  def show(conn, %{"id" => id}) do
    metric = Repo.get(Metric, id)
    render conn, "show.html", metric: metric
  end

  def edit(conn, %{"id" => id}) do
    metric = Repo.get(Metric, id)
    changeset = Metric.changeset(metric)
    render conn, "edit.html", metric: metric, changeset: changeset
  end

  def update(conn, %{"id" => id, "metric" => metric_params}) do
    metric = Repo.get(Metric, id)
    changeset = Metric.changeset(metric, metric_params)

    if changeset.valid? do
      Repo.update(changeset)

      conn
      |> put_flash(:info, "Metric updated successfully.")
      |> redirect(to: metric_path(conn, :index))
    else
      render conn, "edit.html", metric: metric, changeset: changeset
    end
  end

  def delete(conn, %{"id" => id}) do
    metric = Repo.get(Metric, id)
    Repo.delete(metric)

    conn
    |> put_flash(:info, "Metric deleted successfully.")
    |> redirect(to: metric_path(conn, :index))
  end
end
