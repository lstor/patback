defmodule Patback.FeedbackController do
  use Patback.Web, :controller

  plug :scrub_params, "metric" when action in [:create, :update]
  plug :action

  def index(conn, _params) do
    metrics = Repo.get_metrics
    render conn, "index.html", metrics: metrics
  end
end
