defmodule Patback.Answer do
  use Patback.Web, :model

  schema "answers" do
    has_one :metric, Metric
    field :session, :string
    field :value, :string
    field :text, :string

    timestamps
  end

  @required_fields ~w(metric session value)
  @optional_fields ~w(text)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If `params` are nil, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ nil) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
