defmodule Patback.Router do
  use Phoenix.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Patback do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/credits", CreditController, :index
    get "/feedback", FeedbackController, :index

    resources "/answers", AnswerController
    resources "/events",  EventController
    resources "/metrics", MetricController
  end

  # Other scopes may use custom stacks.
  # scope "/api", Patback do
  #   pipe_through :api
  # end
end
