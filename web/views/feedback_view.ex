defmodule Patback.FeedbackView do
  use Patback.Web, :view

  import Patback.LayoutView, only: [glyphicon: 2]

  def feedback_entry(metric_type) do
    render metric_type <> ".html"
  end
end
