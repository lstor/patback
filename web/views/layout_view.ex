defmodule Patback.LayoutView do
  use Patback.Web, :view
  require Logger

  def glyphicon(glyph, opts \\ []) do
    label = Keyword.get(opts, :label, "")
    safe ("<span class=\"glyphicon glyphicon-#{glyph}\"></span>&ensp;" <> label)
  end
end
