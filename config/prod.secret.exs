use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
config :patback, Patback.Endpoint,
  secret_key_base: "i+4/DlfVCj8GUBOeDpClIq6DUDWYPDDi6dgE6ToTJpnsBi40F7IRBIYemHycHL58"

# Configure your database
config :patback, Patback.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: {:system, "DATABASE_URL"}
  #username: "postgres",
  #password: "postgres",
  #database: "patback_prod"
