defmodule Patback.Repo do
  use Ecto.Repo, otp_app: :patback

  import Ecto.Query, only: [from: 2]

  alias Patback.Metric
  alias Patback.Repo

  def get_metrics(count \\ 5, seed \\ nil) do
    if seed == nil, do: seed = :os.timestamp
    :random.seed(seed) # For Enum.shuffle

    query = from m in Metric, select: m

    Repo.all(query)
    |> Enum.shuffle
    |> Enum.take count
  end
end
