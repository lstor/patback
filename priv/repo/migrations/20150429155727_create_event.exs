defmodule Patback.Repo.Migrations.CreateEvent do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :name, :string
      add :description, :string
      add :location, :string
      add :from, :datetime
      add :to, :datetime

      timestamps
    end
  end
end
