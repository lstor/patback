defmodule Patback.Repo.Migrations.CreateMetric do
  use Ecto.Migration

  def change do
    create table(:metrics) do
      add :name, :string
      add :description, :string
      add :type, :string

      timestamps
    end
  end
end
