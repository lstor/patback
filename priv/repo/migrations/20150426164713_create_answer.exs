defmodule Patback.Repo.Migrations.CreateAnswer do
  use Ecto.Migration

  def change do
    create table(:answers) do
      add :metric, references(:metrics)
      add :session, :string
      add :value, :string
      add :text, :string

      timestamps
    end
  end
end
