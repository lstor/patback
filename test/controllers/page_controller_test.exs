defmodule Patback.PageControllerTest do
  use Patback.ConnCase

  test "that opening the main page redirects to /feedback" do
    conn = get conn(), "/"
    assert conn.resp_body =~ ~r[You are being.*/feedback.*redirected]
  end
end
