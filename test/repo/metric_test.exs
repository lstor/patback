defmodule Patback.RepoTest do
  use ExUnit.Case
  alias Patback.Metric
  alias Patback.Repo

  setup do
    insert %{name: "metric-1", type: "yes-no"}
    insert %{name: "metric-2", type: "yes-or-whynot"}
    insert %{name: "metric-3", type: "less-more"}
    insert %{name: "metric-4", type: "less-more"}
    insert %{name: "metric-5", type: "yes-no"}
    insert %{name: "metric-6", type: "yes-or-whynot"}
    insert %{name: "metric-7", type: "yes-or-whynot"}
    insert %{name: "metric-8", type: "yes-no"}
    insert %{name: "metric-9", type: "less-more"}
    :ok
  end

  test "that we get 'count' metrics" do
    for count <- 1..5 do
      metrics = Repo.get_metrics(count)
      assert length(metrics) == count
    end
  end

  test "that changing the seed changes the result" do
    with_first_seed  = Repo.get_metrics(1, {1, 2, 3})
    with_second_seed = Repo.get_metrics(1, {1, 0, 0})

    assert with_first_seed != with_second_seed
  end

  defp insert(params) do
    import Patback.Metric, only: [changeset: 2]

    metric = %Metric{description: "desc"}
    Repo.insert changeset(metric, params)
  end
end
