# Patback

[![Build Status](https://semaphoreci.com/api/v1/projects/78903ac5-1157-4353-b5d7-472ee43c0853/408522/badge.svg)](https://semaphoreci.com/lstor/patback)

To start your new Phoenix application:

1. Install dependencies with `mix deps.get`
2. Start Phoenix endpoint with `mix phoenix.server`

Now you can visit `localhost:4000` from your browser.
